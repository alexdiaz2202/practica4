package practicacuatro;
/**
 * 
 * @author Alexandro Tapia
 * @author Oliver Sanchez
 * @param <E>
 */
public class ArbolBinarioBusqueda<E extends Comparable<E>> extends ArbolBinario{

    /**
     * 
     * @param raiz 
     */
    public ArbolBinarioBusqueda(Nodo raiz) {
        super(raiz);
    }
    
    /**
     * metodo que inserta elementos individuales
     * elementoaux vuelve a el elemento de la raiz en comparable
     * para poder aplicar el metodo compareTo()
     * @param elemento 
     */
    public void insertar(E elemento){
        E elementoaux;
        elementoaux = (E) raiz.getElemento();
        if(raiz == null)
            raiz = new Nodo<>(elemento);
        else if(elementoaux.compareTo(elemento)==0) {
        } else if(elementoaux.compareTo(elemento)>0){
            if(raiz.getIzquierdo()!=null)
                insertar(raiz.getIzquierdo(), elemento);
            else
                raiz.izquierdo = new Nodo<>(elemento);
        }else{
            if(raiz.getIzquierdo()!=null)
                insertar(raiz.getIzquierdo(), elemento);
            else
                raiz.izquierdo = new Nodo<>(elemento);     
        }
    }
    
    
    /**
     * El metodo insertar auxiliar  que de nuevo 
     * inserta elemento pero en una parte distinta del nodo
     * ya sea
     * @param nodo
     * @param elemento 
     */
    public void insertar(Nodo<E> nodo, E elemento){
        if(nodo!=null){
            if(nodo.getElemento().compareTo(elemento)>0){
                if(nodo.getIzquierdo()!=null)
                    insertar(nodo.izquierdo, elemento);
                else
                    nodo.izquierdo = new Nodo<>(elemento);
            }else{
                if(nodo.getDerecho()!=null)
                    insertar(nodo.derecho, elemento);
                else
                    nodo.derecho = new Nodo<>(elemento);
            }
        }else{
            return;
        }   
    }
    @Override
    public String toString() {
        return crearRepresentacion(raiz, "", "", false);
    }

    private String crearRepresentacion(Nodo<E> nodo, String representacion, 
            String nivel, boolean esIzquierdo) {
        
        representacion += nivel;
                
        if (!nivel.equals("")) {
            representacion += "\b\b" + (esIzquierdo ? "\u251C": "\u2514") + "\u2500";
        }
        
        if(nodo == null)
            return representacion += "\n";
        
        representacion += nodo + "\n";
        
        // Hijo izquierdo
        representacion = crearRepresentacion(nodo.getIzquierdo(), representacion, nivel + "\u2502 ", true);
        // Hijo derecho
        representacion = crearRepresentacion(nodo.getDerecho(), representacion, nivel + "  ", false);
        
        return representacion;
    }

}