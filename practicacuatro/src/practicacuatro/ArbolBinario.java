package practicacuatro;
/**
 *
 * @author Alexandro Tapia
 * @author Oliver Sanchez
 * @param <E>
 */
public class ArbolBinario<E> extends Nodo {
    Nodo<E> raiz; 

    /**
     * Constructor por parametros de arbol que recibe un nodo
     * @param raiz 
     */
    public ArbolBinario(Nodo<E> raiz) {
        this.raiz = raiz;
    }

    /**
     * 
     * @return 
     */
    public Nodo<E> getRaiz() {
        return raiz;
    }

    /**
     * 
     * @param raiz 
     */
    public void setRaiz(Nodo<E> raiz) {
        this.raiz = raiz;
    }
    
    
    /**
     *  Metodo el cual hace la visita al nodo, lo que significa imprimir el elemento
     * @param raiz 
     */
    public void visitar(Nodo <E> raiz){
        System.out.println(raiz.getElemento());
    }
    
    /**
     * 
     */
    public void recorrerPreorden(){
        recorrerPreorden(raiz);
    }
    
    
    /**
     * 
     * @param raiz
     */
    public void recorrerPreorden(Nodo<E> raiz){
        if(raiz == null){
            return;
        }
        visitar(raiz);
        recorrerPreorden(raiz.getIzquierdo());
        recorrerPreorden(raiz.getDerecho());
    }
    
    /**
     * 
     */
    public void recorrerInorden(){
        recorrerInorden(raiz);
    }
    
    
    /**
     * 
     * @param raiz
     */
    public void recorrerInorden(Nodo <E> raiz){
        if(raiz == null){
            return;
        }
        recorrerPreorden(raiz.getIzquierdo());
        visitar(raiz);
        recorrerPreorden(raiz.getDerecho());
    }
    
    /**
     * 
     */
    public void recorrerPostorden(){
        recorrerPostorden(raiz);
    }
    
    
    /**
     * 
     * @param raiz
     */
    public void recorrerPostorden(Nodo <E> raiz){
        if(raiz == null){
            return;
        }
        recorrerPreorden(raiz.getIzquierdo());
        recorrerPreorden(raiz.getDerecho());
        visitar(raiz);
    }
    
        @Override
    public String toString() {
        return crearRepresentacion(raiz, "", "", false);
    }

    /**
     * 
     * @param nodo
     * @param representacion
     * @param nivel
     * @param esIzquierdo
     * @return 
     */
    private String crearRepresentacion(Nodo<E> nodo, String representacion, String nivel, boolean esIzquierdo) {
        representacion += nivel;
        
        if (!nivel.equals("")) {
            representacion += "\b\b" + (esIzquierdo ? "\u251C": "\u2514") + "\u2500";
        }
        
        if(nodo == null)
            return representacion += "\n";
        
        representacion += nodo + "\n";
        
        // Hijo izquierdo
        representacion = crearRepresentacion(nodo.getIzquierdo(), representacion, nivel + "\u2502 ", true);
        // Hijo derecho
        representacion = crearRepresentacion(nodo.getDerecho(), representacion, nivel + "  ", false);
        
        return representacion;
    }
    
}
