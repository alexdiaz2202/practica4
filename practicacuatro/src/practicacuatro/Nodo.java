package practicacuatro;
/**
 *
 * @author Alexandro Tapia
 * @author Oliver Sanchez
 * @param <E> objeto en el nodo vial
 */
public class Nodo<E> {
    public E elemento;
    public Nodo<E> izquierdo;
    public Nodo<E> derecho;
    int altura;

    /**
     * Constructor por omision que crea un nodo con dos hijos vacíos
     */
    public Nodo() {
        izquierdo = null;
        derecho = null;
        
    }    
    
    /**
     * Constructor por parametros que crea un nodo con dos hijos predefinidos
     * @param izquierdo nodo izquierdo
     * @param derecho nodo derecho
     */
    public Nodo(Nodo<E> izquierdo, Nodo<E> derecho) {
        this.izquierdo = izquierdo;
        this.derecho = derecho;
    }
    
    /**
     * Constructor que con su parametro crea un nodo con ese elemento
     * @param elemento 
     */
    public Nodo(E elemento){
        this.elemento = elemento;
    }
    
    /**
     * 
     * @param izquierdo
     * @param elemento
     * @param derecho 
     */
    public Nodo(Nodo <E> izquierdo, E elemento, Nodo<E> derecho){
        this.izquierdo = izquierdo;
        this.elemento = elemento;
        this.derecho = derecho;
    }
    
    /**
     * 
     * @return 
     */
    public E getElemento() {
        return elemento;
    }
    
    /**
     * 
     * @param elemento 
     */
    public void setElemento(E elemento) {
        this.elemento = elemento;
    }
    
    /**
     * 
     * @return 
     */
    public Nodo<E> getIzquierdo() {
        return izquierdo;
    }
    
    /**
     * 
     * @param izquierdo 
     */
    public void setIzquierdo(Nodo<E> izquierdo) {
        this.izquierdo = izquierdo;
    }
    
    /**
     * 
     * @return 
     */
    public Nodo<E> getDerecho() {
        return derecho;
    }
    
    /**
     * 
     * @param derecho 
     */
    public void setDerecho(Nodo<E> derecho) {
        this.derecho = derecho;
    }    

    /**
     * 
     * @return 
     */
    public int getAltura() {
        return altura;
    }

    /**
     * 
     * @param altura 
     */
    public void setAltura(int altura) {
        this.altura = altura;
    }
    
    @Override
    public String toString (){
        return "Nodo"+ getElemento(); 
    }
    
}
