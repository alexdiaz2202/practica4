package practicacuatro;
/**
 * 
 * @author Alexandro Tapia
 * @author Oliver Sanchez
 * @param <E>
 */
public class ArbolAVL <E extends Comparable<E>> extends ArbolBinarioBusqueda<E> {

    /**
     * 
     * @param raiz 
     */
    public ArbolAVL(NodoAVL<E> raiz) {
        super(raiz);
    }
    
    /**
     * 
     * @param raiz
     * @return 
     */
    public int altura(NodoAVL<E> raiz) {
        if(raiz == null)
            return -1;
        else
            return raiz.factorEquilibrio;
    }
    
    /**
     * 
     * @param raiz
     * @param raiz2
     * @return 
     */
    public int altura(NodoAVL<E> raiz, NodoAVL<E> raiz2) {
        return Math.max(altura(raiz), altura(raiz2)) + 1;
    }
    
    /**
     * 
     * @param elemento 
     */
    @Override
    public void insertar(E elemento){
            E elementoaux;
            elementoaux = (E) raiz.getElemento();
        if(raiz == null)
            raiz = new NodoAVL(elemento);
        else if(elementoaux.compareTo(elemento)== 0) {
        } else if(elementoaux.compareTo(elemento) > 0){
            if(raiz.getIzquierdo() != null)
                insertar(raiz.getIzquierdo(), elemento);
            else
                raiz.izquierdo = new NodoAVL<>(elemento);
        }else{
            if(raiz.getIzquierdo()!= null)
                insertar(raiz.getIzquierdo(), elemento);
            else
                raiz.izquierdo = new NodoAVL<>(elemento);     
        }
    }
    
    /**
     * 
     * @param nodo
     * @param elemento  
     * @return   
     */
    public NodoAVL<E> insertar(NodoAVL<E> nodo, E elemento){
        if(nodo != null){
            if (elemento.compareTo(nodo.elemento) > 0){
            nodo.derecho = insertar((NodoAVL<E>)nodo.derecho, elemento);
            if(altura((NodoAVL<E>)nodo.izquierdo) - altura((NodoAVL<E>)nodo.derecho) == -2){
                if(elemento.compareTo(nodo.derecho.elemento) > 0){
                    nodo = rotacionIzq(nodo);
                } else {
                    nodo.derecho = rotacionIzq((NodoAVL<E>)nodo.derecho);
                    nodo = rotacionDer(nodo);
                }
            }
        }
         else if (elemento.compareTo(nodo.elemento) < 0) {
                nodo.izquierdo = insertar((NodoAVL<E>)nodo.izquierdo, elemento);
                if((altura((NodoAVL<E>)nodo.izquierdo) - altura((NodoAVL<E>)nodo.derecho)) == 2){
                    if(elemento.compareTo(nodo.izquierdo.elemento) < 0){
                        nodo = rotacionIzq(nodo);
                    } else { 
                        nodo.izquierdo = rotacionDer((NodoAVL<E>)nodo.izquierdo);
                        nodo = rotacionIzq(nodo);
                    }
                }
            }
    } else{
            nodo = new NodoAVL<>(elemento);
        }
        nodo.factorEquilibrio = altura((NodoAVL<E>)nodo.izquierdo, (NodoAVL<E>)nodo.derecho);
        return nodo;
    }
    
    /**
     * 
     * @param raiz 
     * @return  
     */
    public NodoAVL<E> rotacionDer(NodoAVL<E> raiz){
            NodoAVL<E> raizAVL = (NodoAVL<E>)raiz.izquierdo;
            NodoAVL<E> aux = (NodoAVL<E>)raiz.izquierdo.derecho;
            raizAVL.izquierdo = raiz.izquierdo.izquierdo;
            raizAVL.derecho = raiz;
            raiz.derecho = aux;
            raiz.factorEquilibrio = altura((NodoAVL<E>)raiz.izquierdo, (NodoAVL<E>)raiz.derecho);
            raizAVL.factorEquilibrio = Math.max(altura((NodoAVL<E>)raizAVL.derecho), raiz.factorEquilibrio) + 1 ;
            
            return raizAVL;
    }
    
    /**
     * 
     * @param raiz 
     * @return  
     */
    public NodoAVL<E> rotacionIzq(NodoAVL<E> raiz){
            NodoAVL<E> raizAVL = (NodoAVL<E>)raiz.derecho;
            NodoAVL<E> aux = (NodoAVL<E>)raiz.derecho.izquierdo;
            raizAVL.derecho = raiz.derecho.derecho;
            raizAVL.izquierdo = raiz;
            raiz.derecho = aux;
            raiz.factorEquilibrio = altura((NodoAVL<E>)raiz.izquierdo, (NodoAVL<E>)raiz.derecho);
            raizAVL.factorEquilibrio = Math.max(altura((NodoAVL<E>)raizAVL.izquierdo), raiz.factorEquilibrio) + 1 ;
            
            return raizAVL;
    }
}
