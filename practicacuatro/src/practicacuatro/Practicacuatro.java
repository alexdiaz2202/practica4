package practicacuatro;
/**
 *
 * @author alexd
 * @author Oliver Sanchez
 */
public class Practicacuatro {
    public static void main(String[] args) {
         Nodo b = new Nodo(10);
        
        ArbolBinarioBusqueda prueba = new ArbolBinarioBusqueda(b);
        
        prueba.insertar(9);
        prueba.insertar(8);
        prueba.insertar(7);
        prueba.insertar(6);
        prueba.insertar(5);
        prueba.insertar(4);
        prueba.insertar(3);
        prueba.insertar(2);
        prueba.insertar(1);
        prueba.insertar(0);
        prueba.insertar(-1);
        prueba.insertar(-2);
        prueba.insertar(-3);
        prueba.insertar(-4);
        prueba.insertar(-5);
        
        System.out.println("Arbol binario de búsqueda");
        System.out.println();
        System.out.println(prueba);
        System.out.println();
        System.out.println("Recorrido inorden");
        prueba.recorrerInorden(prueba.getRaiz());
        System.out.println();
        System.out.println("Recorrido preorden");
        prueba.recorrerPreorden(prueba.getRaiz());
        System.out.println();
        System.out.println("Recorrido postorden");
        prueba.recorrerPostorden(prueba.getRaiz());
       
        NodoAVL p1 = new NodoAVL(10);
        
        ArbolAVL prueba1 = new ArbolAVL(p1);
        
        prueba1.insertar(9);
        prueba1.insertar(8);
        prueba1.insertar(7);
        prueba1.insertar(6);
        prueba1.insertar(5);
        prueba1.insertar(4);
        prueba1.insertar(3);
        prueba1.insertar(2);
        prueba1.insertar(1);
        prueba1.insertar(0);
        prueba1.insertar(-1);
        prueba1.insertar(-2);
        prueba1.insertar(-3);
        prueba1.insertar(-4);
        prueba1.insertar(-5);
        
        System.out.println("\n");
        
        System.out.println("Arbol AVL");
        System.out.println();
        System.out.println(prueba1);
        System.out.println();
        System.out.println("Recorrido inorden");
        prueba1.recorrerInorden(prueba1.getRaiz());
        System.out.println();
        System.out.println("Recorrido preorden");
        prueba1.recorrerPreorden(prueba1.getRaiz());
        System.out.println();
        System.out.println("Recorrido postorden");
        prueba1.recorrerPostorden(prueba1.getRaiz());
       
    }
    
}
