package practicacuatro;
/**
 * @author Alexandro Tapia
 * @author Oliver Sanchez
 * @param <E>
 */
public class NodoAVL <E> extends Nodo<E>{
    public int factorEquilibrio;

    public NodoAVL(NodoAVL<E> izquierdo, E elemento , NodoAVL<E> derecho){
        super(izquierdo,elemento,derecho);
        altura = 0;  
    } 
    
    /**
     *
     * @param elemento
     */
    public NodoAVL(E elemento) {
        this(null, elemento, null);
    }

    /**
     * 
     * @return 
     */
    public int getFactor() {
        return factorEquilibrio;
    }

    /**
     * 
     * @param nuevoFactor 
     */
    public void setFactor(int nuevoFactor) {
        factorEquilibrio = nuevoFactor;
    }
}
